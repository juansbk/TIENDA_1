-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-12-2019 a las 03:30:48
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_rsu_ultimo`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_auspi_cant` (IN `codAusp` SMALLINT)  BEGIN
	SELECT 
    	auspiciador.cod_auspi,
        auspiciador.nom_auspi,
        auspiciador.id_auspi,
        auspiciador.cant_proy,
        ayuda.fech_ayuda,
        colaboracion.descp_colab
	FROM 
    	auspiciador
        INNER JOIN ayuda on auspiciador.cod_auspi = ayuda.cod_auspi
        INNER JOIN colaboracion on ayuda.cod_ayuda = colaboracion.cod_ayuda
        
    WHERE 
    	ayuda.cod_auspi = codAusp
        order BY auspiciador.cant_proy DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_auspi_col` (IN `codProy` SMALLINT, IN `codAusp` SMALLINT)  BEGIN
	SELECT 
    	auspiciador.cod_auspi,
        auspiciador.nom_auspi,
        auspiciador.id_auspi,
        ayuda.fech_ayuda,
        colaboracion.descp_colab
	FROM 
    	auspiciador
        INNER JOIN ayuda on auspiciador.cod_auspi = ayuda.cod_auspi
        INNER JOIN colaboracion on ayuda.cod_ayuda = colaboracion.cod_ayuda
        
    WHERE 
    	ayuda.cod_proy = codProy and ayuda.cod_auspi = codAusp;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_buscarproy_eje` (IN `eje` SMALLINT)  BEGIN
	SELECT 
    	proyecto.cod_proy,
        proyecto.titu_proy,
        eje_rsu.descp_eje
	FROM 
    	proyecto
        INNER JOIN eje_rsu on proyecto.eje_proy = eje_rsu.id_eje
    WHERE 
    	eje_proy = eje;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_parti_bene` (IN `tipoBene` SMALLINT)  BEGIN
SELECT 
    	participante.cod_part,
        participante.ape_part,
        participante.nom_part,
        participante.id_part,
        beneficiaria.nom_bene
	FROM 
    	participante
        INNER JOIN pertenece_ben on participante.cod_part = pertenece_ben.cod_part
        INNER JOIN beneficiaria on pertenece_ben.cod_bene = beneficiaria.cod_bene
        
    WHERE 
    	pertenece_ben.cod_bene = tipoBene;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_parti_p` (IN `codProy` SMALLINT)  BEGIN
	SELECT 
			participante.cod_part,
            participante.nom_part,
            participante.ape_part,
            participante.id_part
	FROM 
    	participante
        INNER JOIN ejecutar on participante.cod_part = ejecutar.cod_part
        INNER JOIN actividad on ejecutar.cod_activ = actividad.cod_activ
        INNER JOIN realiza on actividad.cod_activ = realiza.cod_activ
        INNER JOIN proyecto on realiza.cod_proy = proyecto.cod_proy
    WHERE 
    	realiza.cod_proy=codProy;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_perApoy_tipo` (IN `tipocApoy` SMALLINT)  BEGIN
	SELECT 
    	personal_apoyo.cod_apoy,
        personal_apoyo.ape_apoy,
        personal_apoyo.nom_apoy,
        personal_apoyo.id_apoy,
        tipo_car_apoy.descp_car_apoy
	FROM 
    	personal_apoyo
        INNER JOIN tipo_car_apoy on personal_apoyo.cod_ti_car_apoy = tipo_car_apoy.cod_ti_car_apoy
    WHERE 
    	personal_apoyo.cod_ti_car_apoy = tipocApoy;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `cod_activ` smallint(6) NOT NULL,
  `descp_activ` varchar(500) NOT NULL,
  `cod_tipo_activ` smallint(6) NOT NULL,
  `fech_ini_activ` date NOT NULL,
  `fech_fin_activ` date NOT NULL,
  `cant_tare` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`cod_activ`, `descp_activ`, `cod_tipo_activ`, `fech_ini_activ`, `fech_fin_activ`, `cant_tare`) VALUES
(1, 'Difusión de la campaña en el campo universitario UNJBG.', 1, '2019-04-25', '2019-05-01', 0),
(2, 'Fabricacion de contenedores', 2, '2019-04-27', '2019-05-02', 1),
(3, 'Realizar compaña de recleccion de residuos de aparatos electronicos', 3, '2019-05-03', '2019-06-20', 0),
(4, 'Realizar  difusion de compaña de alfabetizacion digital para adultos mayores: por sectores ', 1, '2019-05-08', '2019-06-20', 0),
(5, 'Realizar compaña de alfabetizacion digital para adultos mayores sector 1', 1, '2019-06-20', '2019-06-27', 0),
(6, 'Realizar compaña de alfabetizacion digital para adultos mayores sector 2', 1, '2019-06-28', '2019-07-05', 0),
(7, 'Realizar compaña de alfabetizacion digital para adultos mayores sector 3', 1, '2019-07-06', '2019-07-12', 0),
(8, 'Realizar compaña de alfabetizacion digital para adultos mayores sector 4', 1, '2019-07-13', '2019-07-26', 1),
(9, 'Realizar El Mantenimiento preventivo y correctivo de computadoras de la ESIS lab A', 1, '2019-07-13', '2019-07-26', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `antecedente`
--

CREATE TABLE `antecedente` (
  `cod_ante` smallint(6) NOT NULL,
  `fe_ini_ante` date NOT NULL,
  `descp_ante` varchar(500) NOT NULL,
  `titu_ante` varchar(200) NOT NULL,
  `fe_fin_ante` date DEFAULT NULL,
  `cod_proy` smallint(6) NOT NULL,
  `tipo_ante` varchar(20) DEFAULT NULL,
  `pais_ante` varchar(20) NOT NULL,
  `respons_ante` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `antecedente`
--

INSERT INTO `antecedente` (`cod_ante`, `fe_ini_ante`, `descp_ante`, `titu_ante`, `fe_fin_ante`, `cod_proy`, `tipo_ante`, `pais_ante`, `respons_ante`) VALUES
(1, '2019-03-27', 'Promover en la población el concepto de desarrollo sustentable y la importancia de la correcta gestión de aparatos electrónicos e informáticos (AEE´s) al finalizar su ciclo de vida útil o comercial; Difundir el concepto de las “4R” (reducir, reutilizar, reciclar y revalorizar) en relación con la gestión de los electrónicos en desuso', 'RECICLAJE Y REUTILIZACIÓN DE AP ELECTRICOS Y ELECTRONICOS', '2019-03-28', 1, 'CAMPAÑA', 'Argentina', 'Municipalidad Baradero'),
(2, '2019-01-20', 'La campaña tiene como objetivo concientiziar a los estudiantes universitarios sobre la importancia del adecuado reciclaje de los residuos de aparatos eléctricos y electrónicos (RAEE), como un elemento clave para el impulso de un modelo de economía circular que contribuya a la sostenibilidad del medio ambiente.', 'RECICLAJE DE RESIDUOS ELECTRÓNICOS EN UNIVERSIDADES', '2019-01-24', 1, 'CAMPAÑA', 'España', 'Universidad de Zaragoza'),
(3, '2016-09-20', 'El objetivo principal fue difundir y sensibilizar a la ciudadanía sobre la importancia del manejo y disposición adecuada de estos residuos, con la finalidad que no sean descartados a través del servicio de recolección municipal.', 'USAT SE UNE A LA CAMPAÑA REGIONAL DE RECICLAJE ELECTRONICO', '2016-09-27', 1, 'PROYECTO', 'Peru', 'Universidad Santo Toribio'),
(4, '2018-06-04', 'Con el objetivo de poner en práctica la conciencia ambiental en el campus, te invitamos a traer residuos de aparatos eléctricos y electrónicos (RAEE) para ser reciclados. Los RAEE son altamente contaminantes, y saber desecharlos es muy importante.', 'RECOLECCIÓN DE RESIDUOS DE APARATOS ELÉCTRICOS Y ELECTRÓNICOS (RAEE)', '2018-06-07', 1, 'CAMPAÑA', 'Peru', 'Universidad de Lima'),
(5, '2018-10-01', 'Se recibio la capacitación tanto profesores como alumnos sobre la importancia del recojo de estos residuos que son considerados como residuos peligrosos debido al contenido de sustancias potencialmente peligrosas que pueden dar origen a importantes problemas medioambientales, si no son descontaminados adecuadamente.', 'RECICLAJE DE APARATOS ELECTRONICOS Y ELECTRICO (RAEE)', '2018-10-07', 1, 'PROYECTO', 'Peru', 'Sain Exupery'),
(6, '2018-01-08', 'Las razones por las que los adultos mayores aprenden el uso de la computadora son diversas, como adquirir nuevos conocimientos y elevar la autoestima; otras responden a necesidades más concretas, como prepararse para ayudar a sus hijos o nietos en sus estudios, conseguir un nuevo empleo o mantenerse en el actual.', 'ALFABETIZACION DIGITAL PARA ADULTOS MAYORES', '2018-03-12', 2, 'CAMPAÑA', 'Peru', 'Universidad Catolica Santa Maria'),
(7, '2016-08-08', 'El objetivo del programa es desarrollar conocimientos de nuevas tecnologías en los adultos mayores de la ciudad de Arequipa a fin de lograr una mejor integración a la sociedad de la información y mejorar su calidad de vida.', 'PROGRAMA DE INTERVENCION DESARROLLO DE HABILIDADES TECNOLOGICAS EN EL ADULTO MAYOR', '2016-08-30', 2, 'PROGRAMA', 'Peru', 'Universidad San Marcos'),
(8, '2018-12-14', 'Hay que empezar a dejar atrás, la visión de la vejez, como una etapa negativa en la vida y ponernos como rol esencial de la sociedad, mejorar sus condiciones y en este caso, entregarles las herramientas básicas requeridas para adaptarse a un ritmo de vida cada vez más digital y acelerado.', 'ALFABETIZACION DIGITAL DEL ADULTO MAYOR', '2018-12-14', 2, 'TALLER', 'Chile', 'Universidad de Santiago de Chile'),
(9, '2011-10-11', 'Las computadoras son una herramienta muy versátil. Así como son las aliadas de científicos, ingenieros, empresarios y otros profesionales; también son fuente de diversión para jóvenes y adultos; y también poseen una multiplicidad de aplicaciones en el hogar.', 'MANTENIMIENTO PREVENTIVO Y CORRECTIVO DE COMPUTADORAS', '2011-10-23', 3, 'CAMPAÑA', 'Panama', 'Universidad de Panama'),
(10, '2015-08-20', 'Mantener en óptimas condiciones el funcionamiento de los equipos de cómputo de la Universidad del Cauca asegurando así la eficiente operación de los mismos.', 'MANTENIMIENTO PREVENTIVO DE EQUIPOS DE SISTEMAS E INFORMATICA', '2015-08-28', 3, 'CAMPAÑA', 'Colombia', 'Universidad del Cauca'),
(11, '2018-12-14', 'Hay que empezar a dejar atrás, la visión de la vejez, como una etapa negativa en la vida y ponernos como rol esencial de la sociedad, mejorar sus condiciones y en este caso, entregarles las herramientas básicas requeridas para adaptarse a un ritmo de vida cada vez más digital y acelerado.', 'ALFABETIZACION DIGITAL DEL ADULTO MAYOR', '2018-12-14', 2, 'TALLER', 'Chile', 'Universidad de Santiago de Chile'),
(12, '2011-10-11', 'Las computadoras son una herramienta muy versátil. Así como son las aliadas de científicos, ingenieros, empresarios y otros profesionales; también son fuente de diversión para jóvenes y adultos; y también poseen una multiplicidad de aplicaciones en el hogar.', 'MANTENIMIENTO PREVENTIVO Y CORRECTIVO DE COMPUTADORAS', '2011-10-23', 3, 'CAMPAÑA', 'Panama', 'Universidad de Panama'),
(13, '2015-08-20', 'Mantener en óptimas condiciones el funcionamiento de los equipos de cómputo de la Universidad del Cauca asegurando así la eficiente operación de los mismos.', 'MANTENIMIENTO PREVENTIVO DE EQUIPOS DE SISTEMAS E INFORMATICA', '2015-08-28', 3, 'CAMPAÑA', 'Colombia', 'Universidad del Cauca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auspiciador`
--

CREATE TABLE `auspiciador` (
  `cod_auspi` smallint(6) NOT NULL,
  `nom_auspi` varchar(50) NOT NULL,
  `repre_auspi` varchar(40) DEFAULT 'ninguno',
  `id_auspi` varchar(15) NOT NULL,
  `direc_auspi` varchar(60) NOT NULL,
  `cant_proy` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auspiciador`
--

INSERT INTO `auspiciador` (`cod_auspi`, `nom_auspi`, `repre_auspi`, `id_auspi`, `direc_auspi`, `cant_proy`) VALUES
(1, 'Clinica Dental Tejada', 'Carlos Chacon Tejada', '20601723400', 'Av. Bolognesi Nro. 611 Int. 101 Esquina con Miller', 1),
(2, 'Polleria Pollo Pechugon', 'Marisol Robles Quispe', '20119206163', 'Av. Bolognesi Nro. 372', 1),
(3, 'Municipalidad Provincial de Tacna - Tesoreria', 'Juan David Alcantara Martinez', '20147797100', 'Cal. Inclan Nro. 404', 3),
(4, 'Pedro Perez Nieto', 'ninguno', '44782948', 'Asoc Los Robles Mz R-4 Lte 45', 2),
(5, 'Juan Pablo Apaza Choque', 'ninguno', '73923840', 'Av San Martin 234', 1),
(6, 'Rosa Maria Estrada Carrasco', 'ninguno', '00237754', 'Cal. Apurimac Nro. 107', 1),
(7, 'Agencia de Viajes LaFayette', 'Agueda Zevallos Cazorla', '20519976545', 'Calle Bolivar Nro 230', 1),
(8, 'Perfumeria Jose Luis', 'Leandro Mamani Aquise', '20205831771', 'Av. Pinto Nro 234 cerca a Polvos Rosados', 1),
(9, 'Gimnasio Van Fitness', 'Milagros Soto Perez', '20601712340', 'Av. Humboldt 2da. cuadra', 1),
(10, 'DAYPER-PERU E.I.R.L.', 'Octavio Sanchez Jines', '20532470162', 'Cal. Alto de Lima Nro. 1701 Esquina con Calle Piura', 1),
(11, 'Empresa Textil Poppers', 'Guillermo Barca', '20114562198', 'No Especifico Direccion', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ayuda`
--

CREATE TABLE `ayuda` (
  `cod_ayuda` smallint(6) NOT NULL,
  `cod_proy` smallint(6) NOT NULL,
  `cod_auspi` smallint(6) NOT NULL,
  `fech_ayuda` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ayuda`
--

INSERT INTO `ayuda` (`cod_ayuda`, `cod_proy`, `cod_auspi`, `fech_ayuda`) VALUES
(1, 1, 1, '2019-05-04'),
(2, 1, 2, '2019-05-05'),
(3, 1, 8, '2019-05-11'),
(4, 2, 4, '2019-06-15'),
(5, 2, 3, '2019-06-17'),
(6, 2, 7, '2019-06-24'),
(7, 2, 5, '2019-06-28'),
(8, 3, 9, '2019-07-11'),
(9, 3, 6, '2019-07-12'),
(10, 3, 10, '2019-07-22'),
(11, 1, 3, '2019-05-22'),
(12, 1, 4, '2019-05-25'),
(13, 3, 3, '2019-07-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficia`
--

CREATE TABLE `beneficia` (
  `cod_benefi` smallint(6) NOT NULL,
  `cod_proy` smallint(6) NOT NULL,
  `cod_bene` smallint(6) NOT NULL,
  `descp_beneficia` varchar(350) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `beneficia`
--

INSERT INTO `beneficia` (`cod_benefi`, `cod_proy`, `cod_bene`, `descp_beneficia`) VALUES
(1, 1, 3, 'El Voluntariado de la UNJBG sera conciente de reciclar los dispositivos'),
(2, 1, 5, 'El Colegio Jorge Chavez sera conciente de reciclar los dispositivos'),
(3, 1, 7, 'Rotaract Club sera conciente de reciclar los dispositivos'),
(4, 2, 4, 'Ayudaremos a la Casa San Jose para que pueda utilizar las Herramientas Tecnologicas, para que puedan comunicarse'),
(5, 2, 6, 'Ayudaremos a la Cesantes y Jubilados para que pueda utilizar las Herramientas Tecnologicas, para que puedan comunicarse'),
(6, 2, 9, 'Ayudaremos a la Casa Hogar Angelica Recharte para que pueda utilizar las Herramientas Tecnologicas, para que puedan comunicarse'),
(7, 3, 10, 'Haremos el Mantenimiento de Computadoras de la Asoc KOLOB de personas Autistas'),
(8, 3, 1, 'Haremos el Mantenimiento de Computadoras de la Asoc de Sordos'),
(9, 3, 2, 'Haremos el Mantenimiento de Computadoras de la Asoc de Personas con Discapacidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficiaria`
--

CREATE TABLE `beneficiaria` (
  `cod_bene` smallint(6) NOT NULL,
  `nom_bene` varchar(40) DEFAULT NULL,
  `direc_bene` varchar(50) DEFAULT NULL,
  `telef_bene` varchar(10) DEFAULT NULL,
  `cant_ben` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `beneficiaria`
--

INSERT INTO `beneficiaria` (`cod_bene`, `nom_bene`, `direc_bene`, `telef_bene`, `cant_ben`) VALUES
(1, 'ASOC. DE SORDOS', 'Calle Los Geranios Nº 65, Urb. Pescaroli', '052-314073', 3),
(2, 'ASOC. DE PERSONAS CON DISCAPACIDAD', 'Av. Jorge Basadre Grohmann Oeste 415', '052-412212', 3),
(3, 'VOLUNTARIADO BASADRINO', 'Avenidad Miraflores S/N, Miraflores ', '052-583000', 5),
(4, 'CASA HOGAR SAN JOSE', 'Calle Arica Nro 238', '052-578330', 0),
(5, 'IE JORGE CHAVEZ', 'Calle las Casuarinas Nro 452', '052-401099', 0),
(6, 'ASOC. CESAN Y JUBILADOS DE EDUCACION', 'Av Manuel A. Odria 895', '052-314782', 0),
(7, 'ROTARACT CLUB TACNA', 'Los Perales 810', '052-504087', 1),
(8, 'CASA HOGAR DE NIÑOS EL BUEN SAMARITANO', 'Nro. S/n Secc. Viñani Asoc.24 de Junio', '952347765', 1),
(9, 'CASA HOGAR ANGELICA RECHARTE', 'Calle Panamá C. 1100 La Esperanza', '052-726952', 3),
(10, 'KOLOB AUSTISMO', 'Av Pinto Nro 1841 Agrup. Sta Rosa', '052-367073', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colaboracion`
--

CREATE TABLE `colaboracion` (
  `cod_colab` smallint(6) NOT NULL,
  `cod_ayuda` smallint(6) NOT NULL,
  `descp_colab` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colaboracion`
--

INSERT INTO `colaboracion` (`cod_colab`, `cod_ayuda`, `descp_colab`) VALUES
(1, 1, '4 Vales de Blancamiento para Sorteo'),
(2, 1, '1 Vales de Descuento en Ortodoncia'),
(3, 3, '10 contenedores de 13 kg'),
(4, 4, '2 Manuales de Introduccion a la Informatica'),
(5, 4, '8 Compendios Cd de Herramientas de Software'),
(6, 5, '3 USB de 32 GB'),
(7, 5, '1 Memoria Externa 1TB'),
(8, 5, '50 cuadernos con estampado de MPT'),
(9, 8, '10 Brochas pequeñas para limpiar'),
(10, 8, '5 brochas grandes para limpiar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo_auspi`
--

CREATE TABLE `correo_auspi` (
  `cod_co_auspi` smallint(6) NOT NULL,
  `cod_auspi` smallint(6) NOT NULL,
  `descp_co_auspi` varchar(40) DEFAULT NULL,
  `area_co_auspi` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correo_auspi`
--

INSERT INTO `correo_auspi` (`cod_co_auspi`, `cod_auspi`, `descp_co_auspi`, `area_co_auspi`) VALUES
(1, 1, 'clinica_tejada@gmail.com', 'Marketing'),
(2, 2, 'pollo_pechugonTacna@gmail.com', 'Administracion'),
(3, 3, 'mpt_2019@gmail.com', 'Tesoreria'),
(4, 4, 'pepe_nieto@gmail.com', ''),
(5, 5, 'jupa_apaza22@gmail.com', ''),
(6, 6, 'rosa11_carrasco@gmail.com', ''),
(7, 7, 'LaFayette_tacna@gmail.com', 'Recepcion'),
(8, 8, 'joseluis_perfumTacna@gmail.com', 'Administracion'),
(9, 9, 'Gimnasio_Van_Fitness@gmail.com', 'Marketing'),
(10, 10, 'DAYPER-PERU_Tacna@gmail.com', 'Administracion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencia_inter`
--

CREATE TABLE `dependencia_inter` (
  `cod_depen_inter` smallint(6) NOT NULL,
  `descp_depen` varchar(150) DEFAULT NULL,
  `cod_proy` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dependencia_inter`
--

INSERT INTO `dependencia_inter` (`cod_depen_inter`, `descp_depen`, `cod_proy`) VALUES
(1, 'Rectorado', 1),
(2, 'Escuela Profesional de Ing. en Informatica y Sistemas', 1),
(3, 'Direccion de Extension, Proyeccion Universitaria', 1),
(4, 'FAIN', 1),
(5, 'Vicerrectorado Académico', 2),
(6, 'Oficina de RRHH', 2),
(7, 'Escuela Profesional de Ing. en Informatica y Sistemas', 2),
(8, 'Vice Rectorado Académico', 3),
(9, 'Dirección Académica de Calidad', 3),
(10, 'Direccion de Extension, Proyeccion Universitaria', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejecutar`
--

CREATE TABLE `ejecutar` (
  `cod_ejec` smallint(6) NOT NULL,
  `cod_activ` smallint(6) NOT NULL,
  `cod_part` int(11) NOT NULL,
  `cod_lug` smallint(6) NOT NULL,
  `cod_invi` int(11) NOT NULL,
  `facilitador_ejec` smallint(6) NOT NULL,
  `fech_ini_ejec` date NOT NULL,
  `fech_fin_ejec` date NOT NULL,
  `cod_estado_proyecto` smallint(6) NOT NULL,
  `asiste_participante_ejec` binary(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ejecutar`
--

INSERT INTO `ejecutar` (`cod_ejec`, `cod_activ`, `cod_part`, `cod_lug`, `cod_invi`, `facilitador_ejec`, `fech_ini_ejec`, `fech_fin_ejec`, `cod_estado_proyecto`, `asiste_participante_ejec`) VALUES
(1, 4, 1, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x31),
(2, 4, 2, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x31),
(3, 4, 3, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x31),
(4, 4, 4, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x31),
(5, 4, 5, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x30),
(6, 4, 6, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x31),
(7, 4, 7, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x30),
(8, 4, 8, 2, 7, 1, '2019-06-28', '2019-06-30', 3, 0x31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eje_rsu`
--

CREATE TABLE `eje_rsu` (
  `id_eje` smallint(30) NOT NULL,
  `descp_eje` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `eje_rsu`
--

INSERT INTO `eje_rsu` (`id_eje`, `descp_eje`) VALUES
(1, 'Gestión Institucional'),
(2, 'Formación'),
(3, 'Investigación'),
(4, 'Participación social');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encargado`
--

CREATE TABLE `encargado` (
  `cod_encar` smallint(6) NOT NULL,
  `cod_tipo_car_encar` smallint(6) NOT NULL,
  `cod_grado_acad` smallint(6) DEFAULT NULL,
  `nombre_encar` varchar(30) NOT NULL,
  `apellido_encar` varchar(30) NOT NULL,
  `correo_encar` varchar(40) NOT NULL,
  `id_encar` varchar(15) NOT NULL,
  `cant_veces_encar` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `encargado`
--

INSERT INTO `encargado` (`cod_encar`, `cod_tipo_car_encar`, `cod_grado_acad`, `nombre_encar`, `apellido_encar`, `correo_encar`, `id_encar`, `cant_veces_encar`) VALUES
(1, 2, 1, 'Luis', 'Quispe', 'lquispe@gmail.com', '00453215', 0),
(2, 2, 3, 'Carlos', 'Sotomayo', 'csotomato@gmail.com', '00468023', 0),
(3, 2, 1, 'Juan', 'Cardenaz', 'jcardenaz@gmail.com', '45321687', 0),
(4, 2, 3, 'Piero', 'Useca', 'puseca@gmail.com', '15489613', 0),
(5, 2, 2, 'Gianfranco', 'Castillo', 'gcastillo@gmail.com', '00421356', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_proyecto`
--

CREATE TABLE `estado_proyecto` (
  `cod_estado_proyecto` smallint(6) NOT NULL,
  `descp_estado_proyecto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado_proyecto`
--

INSERT INTO `estado_proyecto` (`cod_estado_proyecto`, `descp_estado_proyecto`) VALUES
(1, 'POR EJECUTARSE'),
(2, 'EN EJECUCION'),
(3, 'EJECUTADO'),
(4, 'CANCELADO'),
(5, 'SUSPENDIDO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiamiento`
--

CREATE TABLE `financiamiento` (
  `cod_finan` smallint(6) NOT NULL,
  `cod_proy` smallint(6) NOT NULL,
  `fuente_finan` varchar(40) NOT NULL,
  `monto_finan` decimal(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `financiamiento`
--

INSERT INTO `financiamiento` (`cod_finan`, `cod_proy`, `fuente_finan`, `monto_finan`) VALUES
(1, 1, 'Recursos Directamente Recaudados', '300.00'),
(2, 1, 'Financiamiento Escuela', '230.00'),
(3, 2, 'Recursos Ordinarios', '600.00'),
(4, 2, 'Financiamiento Escuela', '130.00'),
(5, 3, 'Autofinanciado', '400.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grado_academico`
--

CREATE TABLE `grado_academico` (
  `cod_grado_acad` smallint(6) NOT NULL,
  `descp_grado_acad` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grado_academico`
--

INSERT INTO `grado_academico` (`cod_grado_acad`, `descp_grado_acad`) VALUES
(1, 'Bachiller'),
(2, 'Magister'),
(3, 'Doctor'),
(4, 'PHd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucion`
--

CREATE TABLE `institucion` (
  `cod_insti` smallint(6) NOT NULL,
  `cod_pais` smallint(6) NOT NULL,
  `nombre_insti` varchar(100) NOT NULL,
  `correo_insti` varchar(50) NOT NULL,
  `direc_insti` varchar(100) NOT NULL,
  `area_insti` varchar(30) NOT NULL,
  `telef_insti` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `institucion`
--

INSERT INTO `institucion` (`cod_insti`, `cod_pais`, `nombre_insti`, `correo_insti`, `direc_insti`, `area_insti`, `telef_insti`) VALUES
(1, 1, 'Universidad Nacional de Trujillo', '@unt.edu.pe', 'Jr. Independencia Nro. 389,San Martín Nro. 344', 'A. Informatica', '044209020'),
(2, 1, 'Universidad Nacional de San Agustin', '@unsa.edu.pe', 'Calle Santa Catalina Nro. 117. Cercado - Arequipa', 'A. Informatica', '054391911'),
(3, 1, 'Universidad Nacional Jorge Basadre Grohmann', '@unjbg.edu.pe', 'Ciudad Universitaria - Av. Miraflores S/N', 'A. Informatica y Sistemas', '052583000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrantes_comite`
--

CREATE TABLE `integrantes_comite` (
  `cod_integ` smallint(6) NOT NULL,
  `nom_integ` varchar(30) NOT NULL,
  `ape_integ` varchar(30) NOT NULL,
  `tipo_integ` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `integrantes_comite`
--

INSERT INTO `integrantes_comite` (`cod_integ`, `nom_integ`, `ape_integ`, `tipo_integ`) VALUES
(1, 'Karin Yanet', 'Supo Gavancho', 'Presidente'),
(2, 'Ana', 'Cori Moron', 'Miembro'),
(3, 'Ronald Daniel', 'Santana Tapia', 'Miembro'),
(4, 'Hugo Manuel', 'Barraza Tapia', 'Miembro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `int_com_proyecto`
--

CREATE TABLE `int_com_proyecto` (
  `cod_int_proy` smallint(6) NOT NULL,
  `cod_integ` smallint(6) NOT NULL,
  `cod_proy` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `int_com_proyecto`
--

INSERT INTO `int_com_proyecto` (`cod_int_proy`, `cod_integ`, `cod_proy`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 1, 2),
(6, 2, 2),
(7, 3, 2),
(8, 4, 2),
(9, 1, 3),
(10, 2, 3),
(11, 3, 3),
(12, 4, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invitado`
--

CREATE TABLE `invitado` (
  `cod_invi` int(11) NOT NULL,
  `cod_grado_acad` smallint(6) NOT NULL,
  `nom_invi` varchar(40) NOT NULL,
  `ape_invi` varchar(40) NOT NULL,
  `correo_invi` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `invitado`
--

INSERT INTO `invitado` (`cod_invi`, `cod_grado_acad`, `nom_invi`, `ape_invi`, `correo_invi`) VALUES
(1, 2, 'Juan Luis', 'Sanchez Carrion', 'jsanchezc@gmail.com'),
(2, 1, 'Carlos Jose', 'Quispe Mamani', 'cquispem@gmail.com'),
(3, 1, 'Luis', 'Santos Gonzales', 'lsantosg@gmail.com'),
(4, 4, 'Milagros Veronica', 'Gonzales Quispe', 'mgonzalezq@gmail.com'),
(5, 2, 'Luis Mario', 'Santos Cardenaz', 'lsantosc@gmail.com'),
(6, 2, 'Pedro Juan', 'Tintaya Carrion', 'ptintayac@gmail.com'),
(7, 1, 'Mariela Luisa', 'Mamani Quispe', 'mmammaniq@gmail.com'),
(8, 4, 'Brayan', 'Palomino Gonzales', 'bpalominog@gmail.com'),
(9, 1, 'Jorge Carlos', 'Laura Quispe', 'jlauraq@gmail.com'),
(10, 2, 'Jhon Luis', 'Benavides Lopez', 'jbenavidezl@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

CREATE TABLE `lugar` (
  `cod_lug` smallint(6) NOT NULL,
  `descp_lug` varchar(30) NOT NULL,
  `locali_lug` varchar(40) NOT NULL,
  `tipo_lug` varchar(40) NOT NULL,
  `aforo_lug` smallint(6) NOT NULL,
  `cond_lug` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`cod_lug`, `descp_lug`, `locali_lug`, `tipo_lug`, `aforo_lug`, `cond_lug`) VALUES
(1, 'Auditorio de Contabilidad', 'Pabellon de Contabilidad', 'Auditorio', 500, 'Gratuito'),
(2, 'Aulas TIC', 'Pabellon de Mecanica', 'Laboratorio', 25, 'Alquilado'),
(3, 'Auditorio Central', 'Biblioteca Central', 'Auditorio', 300, 'Gratuito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medio_verificacion_activ`
--

CREATE TABLE `medio_verificacion_activ` (
  `cod_med_verif_activ` smallint(6) NOT NULL,
  `cod_super_activ` smallint(6) NOT NULL,
  `tipo_med_verif_activ` varchar(20) NOT NULL,
  `url_med_verif_activ` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `medio_verificacion_activ`
--

INSERT INTO `medio_verificacion_activ` (`cod_med_verif_activ`, `cod_super_activ`, `tipo_med_verif_activ`, `url_med_verif_activ`) VALUES
(1, 1, 'Fotos', 'https://www.dropbox.com/individual/651432132'),
(2, 2, 'Fotos', 'https://www.dropbox.com/individual/6213549'),
(3, 3, 'Fotos', 'https://www.dropbox.com/individual/62516549'),
(4, 4, 'Fotos', 'https://www.dropbox.com/individual/6519498'),
(5, 5, 'Fotos', 'https://www.dropbox.com/individual/32190654'),
(6, 5, 'Asistencia', 'https://www.dropbox.com/individual/32190654'),
(7, 6, 'Fotos', 'https://www.dropbox.com/individual/6519498'),
(8, 6, 'Asistencia', 'https://www.dropbox.com/individual/6519498'),
(9, 7, 'Fotos', 'https://www.dropbox.com/individual/6519498'),
(10, 7, 'Asistencia', 'https://www.dropbox.com/individual/6519498'),
(11, 8, 'Fotos', 'https://www.dropbox.com/individual/6519498'),
(12, 8, 'Asistencia', 'https://www.dropbox.com/individual/6519498'),
(13, 9, 'Fotos', 'https://www.dropbox.com/individual/6519498');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medio_verificacion_tare`
--

CREATE TABLE `medio_verificacion_tare` (
  `cod_med_tare` smallint(6) NOT NULL,
  `cod_super_tare` smallint(6) NOT NULL,
  `tipo_med_tare` varchar(20) NOT NULL,
  `url_med_tare` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `medio_verificacion_tare`
--

INSERT INTO `medio_verificacion_tare` (`cod_med_tare`, `cod_super_tare`, `tipo_med_tare`, `url_med_tare`) VALUES
(1, 1, 'Fotos', 'http://www2.inecc.gob.mx'),
(2, 2, 'Videos', 'http://www.santiago.cu/cienciapcm'),
(3, 1, 'Asistencia', 'http://www.fudena.org.ve/problemas.htm'),
(4, 2, 'fotos', 'http://edafologia.fcien.edu.uy/archivos'),
(5, 1, 'Videos', 'https://www.pinterest.es/candelariacuestas/talleres'),
(6, 2, 'Asistencia', 'http://www.nytimes.com/'),
(7, 1, 'Fotos', 'https://docs.google.com/document'),
(8, 2, 'Fotos', 'https://drive.google.com/drive/u/0/my-drive');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `mont_finan`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `mont_finan` (
`titu_proy` varchar(200)
,`fuente_finan` varchar(40)
,`monto_finan` decimal(6,2)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objetivo`
--

CREATE TABLE `objetivo` (
  `cod_obj` smallint(6) NOT NULL,
  `tip_obj` varchar(20) NOT NULL,
  `descp_obj` varchar(300) NOT NULL,
  `cod_proy` smallint(6) NOT NULL,
  `niv_cumpli` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `objetivo`
--

INSERT INTO `objetivo` (`cod_obj`, `tip_obj`, `descp_obj`, `cod_proy`, `niv_cumpli`) VALUES
(1, 'General', 'Concientiziar a las personas sobre la importancia del adecuado reciclaje de los residuos de aparatos eléctricos y electrónicos', 1, '98.00'),
(3, 'Especifico', 'Que las personas que van a participar en el proyecto almnenos reciclen 1 dispositivo elecrico', 1, '60.00'),
(4, 'General', 'Desarrollar conocimientos de nuevas tecnologías en los adultos mayores de la ciudad a fin de lograr una mejor integración en nuestra sociedad', 2, '85.00'),
(5, 'Especifico', 'Que las personas que van a participan puedan comunicarse mediante las aplicaciones con familiares lejanos', 2, '60.00'),
(6, 'General', 'Mantener en óptimas condiciones el funcionamiento de los equipos de computo', 3, '93.00');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `obj_general`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `obj_general` (
`descp_obj` varchar(300)
,`niv_cumpli` decimal(5,2)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `cod_pais` smallint(6) NOT NULL,
  `nombre_pais` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`cod_pais`, `nombre_pais`) VALUES
(1, 'Perú'),
(2, 'Argentina'),
(3, 'Chile'),
(4, 'Bolivia'),
(5, 'Colombia'),
(6, 'Mexico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participante`
--

CREATE TABLE `participante` (
  `cod_part` int(11) NOT NULL,
  `nom_part` varchar(30) NOT NULL,
  `ape_part` varchar(30) NOT NULL,
  `tipo_part` varchar(20) NOT NULL,
  `fecha_nac_part` date NOT NULL,
  `direc_part` varchar(40) NOT NULL,
  `correo_part` varchar(40) DEFAULT NULL,
  `sex_part` binary(1) NOT NULL,
  `id_part` varchar(15) NOT NULL,
  `ciudad` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `participante`
--

INSERT INTO `participante` (`cod_part`, `nom_part`, `ape_part`, `tipo_part`, `fecha_nac_part`, `direc_part`, `correo_part`, `sex_part`, `id_part`, `ciudad`) VALUES
(1, 'Luis', 'Martinez Lopez', 'General', '1959-01-06', 'Av. Miraflores MzA Lt21', '', 0x31, '00459654', 'Tacna'),
(2, 'Juan', 'Sanchez Pari', 'General', '1961-04-07', 'Av Humbolt MzC Lt32', '', 0x31, '00459863', 'Tacna'),
(3, 'Giancarlo', 'Carrizo Castillo', 'General', '1957-03-09', 'J.V. Manrique MzD Lt13', '', 0x31, '00463218', 'Tacna'),
(4, 'Pedro', 'Morales Peralta', 'General', '1959-04-09', 'Av. Molina MzZ Lt16', 'pmoralesp@gmail.com', 0x31, '00651498', 'Tacna'),
(5, 'Marisol', 'Luna Juarez', 'General', '1959-02-06', 'Av. Lima Oeste MzK Lt4', 'mLunaj@gmail.com', 0x30, '00475223', 'Tacna'),
(6, 'Patricia', 'Navarro Vargas', 'General', '1961-05-08', 'JV Castillo MzS Lt17 ', '', 0x30, '01846237', 'Tacna'),
(7, 'Mario', 'Correa Paz', 'General', '1961-09-02', 'Av. Pallardelli MzL Lt1', '', 0x31, '07852346', 'Tacna'),
(8, 'Erik', 'Rivero Maldonado', 'General', '1959-12-04', 'JV Castillo MzS Lt13 ', 'eriverom@gmail.com', 0x31, '24657898', 'Tacna'),
(9, 'Daniel', 'Ojeda Muñoz', 'General', '2000-02-02', 'Av. Pallardelli MzD Lt7', 'dojeda@gmail.com', 0x31, '04532879', 'Tacna'),
(10, 'Francisco', 'Blanco Cruz', 'General', '2000-04-03', 'Av Humbolt MzJ Lt24', 'fblancoc@gmail.com', 0x31, '02148962', 'Tacna'),
(11, 'Carlos', 'Miranda Mansilla', 'Estudiante', '2000-09-22', 'Av. Miraflores MzA Lt24', 'cmirandam@gmail.com', 0x31, '65324892', 'Tacna'),
(12, 'Jose', 'Quispe Romero', 'Docente', '1970-02-03', 'Av. Circunvalacion Nro 456', 'mjqromero@hotmail.com', 0x31, '00481925', 'Tacna'),
(13, 'Marcos', 'Giron Siña', 'General', '1979-05-07', 'Av. Los Sauces Nro 78', 'mgironsiña@hotmail.com', 0x31, '00461473', 'Arequipa'),
(14, 'Teresa', 'Medrano Carazas', 'Docente', '1982-12-07', 'Asoc. Las Gardenias Lte 34', 'teremeca@hotmail.com', 0x30, '72154834', 'Tacna'),
(15, 'Milton', 'Carbajal Cruz', 'General', '1970-02-03', 'Asoc. Las casuarinas Mz 4 lte 78', 'milton_cacr@gmail.com', 0x31, '72364582', 'Arequipa'),
(16, 'Milagros', 'Mendoza Krill', 'General', '1979-06-08', 'Sauces del sol Nro 456', 'mila_men@hotmail.com', 0x30, '04823691', 'Moquegua'),
(17, 'Jesus', 'Pacheco Galvez', 'Estudiante', '2000-07-08', 'Los sicarios del mal Lte 45', 'jesus_gal@hotmail.com', 0x31, '04163491', 'Tacna'),
(18, 'Sebastian', 'Mena Cuoto', 'Estudiante', '2000-08-14', 'Asoc Laz Brisas Nro 11', 'sebas_meuto@hotmail.com', 0x31, '04814701', 'Tacna'),
(19, 'Laura', 'Tellez Gil', 'Estudiante', '1999-11-17', 'Las Viñas Nro 444', 'laute_gil@hotmail.com', 0x30, '04821148', 'Tacna'),
(20, 'Maria', 'Costa Ramirez', 'Estudiante', '1999-12-11', 'Monte Verde Nro 56', 'maria_cos@gmail.com', 0x30, '04146291', 'Tacna'),
(21, 'Monica', 'Quiroz Rodriguez', 'General', '1985-10-16', 'San Pedro NRo 452', 'moni_qui@hotmail.com', 0x30, '04821347', 'Tacna'),
(22, 'Valeria', 'Espejo Sosa', 'General', '1976-06-08', 'Miramar Nro 45', 'espejo_val@hotmail.com', 0x30, '01472691', 'Tacna'),
(23, 'Cristhoper', 'Serna Cabrera', 'General', '1981-07-09', 'Nro 342', 'crito_ser@hotmail.com', 0x31, '01698691', 'Tacna'),
(24, 'Santiago', 'Lopez Clares', 'Docente', '1978-02-28', 'Av El sol 210', 'santi_lop@hotmail.com', 0x31, '04823691', 'Tacna'),
(25, 'Tiana', 'Pacco Cruz', 'Docente', '1977-09-15', 'Av La cultura Lte 48', 'tianaaa_pacc@hotmail.com', 0x30, '04112591', 'Tacna'),
(26, 'Melisa', 'Vargas Bernaola', 'General', '1979-06-08', 'Av. Las Guirnaldas del bien Lte 45', 'melili_var@hotmail.com', 0x30, '004652691', 'Moquegua'),
(27, 'Mariely', 'Ayllon Montes', 'General', '1979-06-08', 'Las Magnolias  Lte 47', 'ayllon_mmm@hotmail.com', 0x30, '00124691', 'Moquegua'),
(28, 'Juan', 'Queque Velasquez', 'General', '1979-06-08', 'Las Acacias Marinas Lte 45', 'jjqueque@hotmail.com', 0x31, '04143691', 'Moquegua');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_apoyo`
--

CREATE TABLE `personal_apoyo` (
  `cod_apoy` smallint(6) NOT NULL,
  `nom_apoy` varchar(30) NOT NULL,
  `ape_apoy` varchar(30) NOT NULL,
  `correo_apoy` varchar(40) NOT NULL,
  `id_apoy` varchar(15) NOT NULL,
  `cod_ti_car_apoy` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personal_apoyo`
--

INSERT INTO `personal_apoyo` (`cod_apoy`, `nom_apoy`, `ape_apoy`, `correo_apoy`, `id_apoy`, `cod_ti_car_apoy`) VALUES
(1, 'Julio', 'Alvarado', 'julgas@gmx.net', '21865440', 1),
(2, 'Alejandro', 'Argumedo', 'andes@andes.org.pe', '9098642', 2),
(3, 'Jorge', 'Cabrera', 'jorgecmedaglia@hotmail.com', '25523664', 3),
(4, 'Maria Jose', 'Calderon', 'jorgecmedaglia@hotmail.com', '86676614', 2),
(5, 'Jorge', 'Cabrera', 'MCalderon@minambiente.gov.co', '25676337', 1),
(6, 'Deyanira', 'Camacho', 'rdcamacho@iepi.gov.ec', '25454452', 1),
(7, 'Mariela', 'Canépa', 'mcanepa@comunidadandina.org', '19811114', 1),
(8, 'Dino', 'Delgado', 'dino@asdmas.com', '23150497', 1),
(9, 'Xavier', 'Romero', 'gerencia@anecacao.com', '19979598', 3),
(10, 'Victoria', 'Elmore', 'velmore@mincetur.gob.pe', '7296320', 2),
(11, 'Rainer', 'Engels', 'rainer.engels@gtz.de', '31619357', 1),
(12, 'Marcela', 'Escobar', 'mescobar@barredamoller.com', '23204150', 2),
(13, 'Mariela', 'Canépa', 'mcanepa@comunidadandina.org', '21104701', 1),
(14, 'Dino', 'Delgado', 'dino@asdmas.com', '25529875', 1),
(15, 'Xavier', 'Romero', 'gerencia@anecacao.com', '16132432', 1),
(16, 'Victoria', 'Elmore', 'velmore@mincetur.gob.pe', '19977910', 1),
(17, 'Rainer', 'Engels', 'rainer.engels@gtz.de', '31031524', 1),
(18, 'Marcela', 'Escobar', 'mescobar@barredamoller.com', '23655129', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pertenece_ben`
--

CREATE TABLE `pertenece_ben` (
  `cod_perte_ben` smallint(6) NOT NULL,
  `cod_part` int(11) NOT NULL,
  `cod_bene` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pertenece_ben`
--

INSERT INTO `pertenece_ben` (`cod_perte_ben`, `cod_part`, `cod_bene`) VALUES
(1, 1, 4),
(2, 2, 4),
(3, 3, 4),
(4, 4, 6),
(5, 5, 6),
(6, 6, 6),
(7, 7, 4),
(8, 8, 6),
(9, 9, 7),
(10, 10, 7),
(11, 11, 3),
(13, 7, 6),
(14, 13, 1),
(15, 15, 2),
(16, 16, 2),
(17, 21, 1),
(18, 17, 3),
(19, 14, 3),
(20, 12, 3),
(21, 18, 3),
(22, 19, 3),
(23, 20, 7),
(24, 21, 9),
(25, 22, 9),
(26, 23, 8);

--
-- Disparadores `pertenece_ben`
--
DELIMITER $$
CREATE TRIGGER `cant_benefic` AFTER INSERT ON `pertenece_ben` FOR EACH ROW BEGIN

UPDATE Beneficiaria set cant_ben=cant_ben+1
WHERE Beneficiaria.cod_bene=NEW.cod_bene;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pertenece_invi`
--

CREATE TABLE `pertenece_invi` (
  `cod_perte_invi` smallint(6) NOT NULL,
  `cod_invi` int(11) NOT NULL,
  `cod_insti` smallint(6) NOT NULL,
  `cargo_invi` varchar(180) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pertenece_invi`
--

INSERT INTO `pertenece_invi` (`cod_perte_invi`, `cod_invi`, `cod_insti`, `cargo_invi`) VALUES
(1, 1, 3, 'Docente del Departamento de Informatica'),
(2, 2, 1, 'Docente del Departamento de Ciencias Sociales'),
(3, 3, 2, 'Director de la Escuela de Ingenieria Ambiental'),
(4, 4, 2, 'Docente del Departamento de Ofimatica'),
(5, 5, 1, 'Psicologo de la Escuela de Psicologia'),
(6, 6, 1, 'Coordinador de Soporte Tecnico'),
(7, 7, 2, 'Docente del Departamento de Informatica'),
(8, 8, 2, 'Director de Telematica'),
(9, 9, 3, 'Docente del Departamento Ciencias Sociales '),
(10, 10, 3, 'Docente del Departamento de Informatica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `cod_proy` smallint(6) NOT NULL,
  `titu_proy` varchar(200) NOT NULL,
  `justi_proy` varchar(1200) NOT NULL,
  `ambi_proy` varchar(100) NOT NULL,
  `pobla_proy` varchar(50) NOT NULL,
  `dirig_proy` varchar(200) NOT NULL,
  `eje_proy` smallint(6) NOT NULL,
  `doc_val_proy` varchar(80) NOT NULL,
  `presu_proy` decimal(6,2) NOT NULL,
  `metod_proy` varchar(500) NOT NULL,
  `fe_ini_proy` date NOT NULL,
  `fe_fin_proy` date NOT NULL,
  `diag_proy` varchar(1500) NOT NULL,
  `resul_proy` varchar(1200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`cod_proy`, `titu_proy`, `justi_proy`, `ambi_proy`, `pobla_proy`, `dirig_proy`, `eje_proy`, `doc_val_proy`, `presu_proy`, `metod_proy`, `fe_ini_proy`, `fe_fin_proy`, `diag_proy`, `resul_proy`) VALUES
(1, 'RECOLECCIÓN DE RESIDUOS DE APARATOS ELÉCTRICOS Y ELECTRÓNICOS', 'Actualmente en nuestra sociedad no se tiene conciencia acerca del reciclado de aparatos electronicos, tampoco se sabe de las consecuencias de arrojarlos en cualquier, es por eso que se toma la iniciativa de atacar este problema.', 'La ciudad Universitaria, en la ciudad de Tacna', '400 personas', 'Estudiantes UNJBG, estudiantes de IE, Asociaciones Juveniles', 1, 'Resol Rectoral Nro 5890-2019-UN/JBG', '530.00', 'Sera tipo Campaña, habra puestos con contenedores para poder reciclar, tambien habra personas que repartiran volantes y explicaran acerca del reciclaje. Habra premios para los que acierten en los sorteos', '2019-04-15', '2019-06-20', 'Se realizo encuestas en el Campus universitario y en el Centro de la ciudad, donde se arrojo que mas del 79% de personas encuestadas no tenian consciencia acerca del reciclado.', 'Se obtuvo un resultado de 98%, es decir cumplimos casi totalmente nuestros objetivos.'),
(2, 'ALFABETIZACION DIGITAL PARA ADULTOS MAYORES', 'Hoy en dia en nuestra localidad hay una gran mayoria de personas de la tercera edad, que desconoce de las herramientas tecnologicas para poder comunicarse o para realizar tareas que hoy en dia son casi necesarias', 'Ciudad de Tacna, Casa Hogar San Jose y otros similares ', '150 personas', 'Personas de la tercera edad', 4, 'Resol Rectoral Nro 5898-2019-UN/JBG', '730.00', 'Tipo Taller, se realizara talleres, donde se les enseñara las diferentes Herramientas Tecnologicas Basicas', '2019-05-08', '2019-07-26', 'Se realizo examenes a los participantes sobre conocimientos basicos para medir en que estado estaban antes del proyecto y arrojo que solo un 33% tenia nociones basicas sobre herramientas Tecnologicas', 'Se obtuvo un resultado de 85%, es decir mas de la mitad de cumplimiento de nuestros objetivos'),
(3, 'MANTENIMIENTO PREVENTIVO Y CORRECTIVO DE COMPUTADORAS', 'Nostros ya que estamos vinculados al area de Informatica, hemos visto por conveniente realizar un buen mantenimiento de los equipos informaticos de nuestra institucion y de otras instituciones, con el fin de poner nuestros conocimientos a prueba y asi ayudar a nuestra comunidad.', 'Ciudad de Tacna, Asociaciones', '200 personas', 'Personas que perteneces a las Asociaciones', 2, 'Resol Rectoral Nro 5903-2019-UN/JBG', '400.00', 'Sera tipo Campaña, iremos a las asociaciones y realizaremos el mantenimiento de los equipos informaticos, tambien se le brindara charlas para el buen uso de los equipos informaticos', '2019-06-01', '2019-08-29', 'Se realizo entrevistas con los representantes de las asociaciones donde se obtuvo que mas de 50% hacia mal uso de los equipos informaticos', 'Se obtuvo un resultado de 93%, ya que se hizo el mantenimiento de todos los equipos informaticos y las charlas.'),
(4, 'ACTIVIDAD INVESTIGATIVA INTEGRA EL ENFOQUE RSU GENERANDO VÍNCULOS CON ACTORES EXTERNOS.', 'Fortalecer la investigación científica, tecnológica y humanística de la comunidad académica', 'La ciudad Universitaria, en la ciudad de Tacna', '150 personas', 'Estudiantes UNJBG', 3, 'Resol Rectoral Nro 5908-2019-UN/JBG', '1000.00', 'Se realizara charlas y capacitaciones', '2019-08-29', '2019-09-30', 'Se realizo un sondeo', 'Se obtuvo un resultado de 98%, es decir cumplimos'),
(5, 'LUCHA CONTRA EL ASISTENCIALISMO, MEDIANTE LA ARTICULACIÓN ENTRE PROFESIONALIZACIÓN Y ACCIÓN SOLIDADRIA, VINCULANDO UNIVERSIDAD-SOCIEDAD', 'Desarrollar responsabilidad social en la comunidad universitaria', 'La ciudad Universitaria, en la ciudad de Tacna', '150 personas', 'Estudiantes UNJBG', 4, 'Resol Rectoral Nro 5956-2019-UN/JBG', '1050.00', 'Se realizara charlas y capacitaciones', '2019-09-30', '2019-10-03', 'Se observo en las escuelas', 'Se obtuvo un resultado de 93%, es decir cumplimos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `realiza`
--

CREATE TABLE `realiza` (
  `cod_reali` smallint(6) NOT NULL,
  `cod_proy` smallint(6) NOT NULL,
  `cod_activ` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `realiza`
--

INSERT INTO `realiza` (`cod_reali`, `cod_proy`, `cod_activ`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1),
(4, 1, 1),
(5, 2, 2),
(6, 3, 3),
(7, 3, 4),
(8, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recursos`
--

CREATE TABLE `recursos` (
  `cod_recur` smallint(6) NOT NULL,
  `tipo_recur` varchar(20) NOT NULL,
  `descp_recur` varchar(60) NOT NULL,
  `cant_recur` smallint(6) NOT NULL,
  `cost_uni_recur` decimal(5,2) NOT NULL,
  `cost_tot_recur` decimal(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recursos`
--

INSERT INTO `recursos` (`cod_recur`, `tipo_recur`, `descp_recur`, `cant_recur`, `cost_uni_recur`, `cost_tot_recur`) VALUES
(1, 'Material', 'Papel volante', 1000, '0.50', '5000.00'),
(2, 'Material', 'Cartones', 100, '1.20', '120.00'),
(3, 'Personal', 'Servicio Tecnico Electrico', 1, '150.00', '150.00'),
(4, 'Material', 'Tijeras', 5, '4.00', '20.00'),
(5, 'Personal', 'Serv Bocatidos', 1, '140.00', '140.00'),
(6, 'Material', 'Pegamento', 25, '1.30', '32.50'),
(7, 'Material', 'Hoja Bond a4', 200, '0.10', '20.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recur_activ`
--

CREATE TABLE `recur_activ` (
  `cod_recur_activ` smallint(6) NOT NULL,
  `cod_activ` smallint(6) NOT NULL,
  `cod_recur` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recur_activ`
--

INSERT INTO `recur_activ` (`cod_recur_activ`, `cod_activ`, `cod_recur`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 1, 1),
(5, 2, 2),
(6, 3, 3),
(7, 4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recur_tare`
--

CREATE TABLE `recur_tare` (
  `cod_recur_tare` smallint(6) NOT NULL,
  `cod_tare` smallint(6) NOT NULL,
  `cod_recur` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recur_tare`
--

INSERT INTO `recur_tare` (`cod_recur_tare`, `cod_tare`, `cod_recur`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 1, 1),
(6, 2, 2),
(7, 3, 3),
(8, 4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requerir`
--

CREATE TABLE `requerir` (
  `cod_requi` smallint(6) NOT NULL,
  `cod_tare` smallint(6) NOT NULL,
  `cod_apoy` smallint(6) NOT NULL,
  `detalle_requi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `requerir`
--

INSERT INTO `requerir` (`cod_requi`, `cod_tare`, `cod_apoy`, `detalle_requi`) VALUES
(1, 1, 1, 'Se requiere internet, un diseador'),
(2, 2, 2, 'Se requiere personal para solicitar permiso'),
(3, 3, 3, 'Se requiere persona para repartir volantes'),
(4, 4, 4, 'Se requiere persona para repartir volantes'),
(5, 1, 1, 'Personal Encargado de busqueda de lugar para el taller '),
(6, 1, 2, 'Personal Encargado de de busqueda de ponente fuera de Tacna'),
(7, 1, 3, 'Peronal Encargado de buscar auspicio'),
(8, 2, 4, 'Se requiere personal para solicitar permiso'),
(9, 3, 5, 'Se requiere persona para repartir volantes'),
(10, 4, 6, 'Se requiere persona para repartir volantes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisa_activ`
--

CREATE TABLE `supervisa_activ` (
  `cod_super_activ` smallint(6) NOT NULL,
  `cod_activ` smallint(6) NOT NULL,
  `supervisor_activ` smallint(6) NOT NULL,
  `fech_super_activ` date DEFAULT NULL,
  `nivel_cumpli_activ` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `supervisa_activ`
--

INSERT INTO `supervisa_activ` (`cod_super_activ`, `cod_activ`, `supervisor_activ`, `fech_super_activ`, `nivel_cumpli_activ`) VALUES
(1, 1, 1, '2019-04-25', '100.00'),
(2, 2, 2, '2019-04-27', '100.00'),
(3, 3, 3, '2019-05-03', '100.00'),
(4, 4, 4, '2019-05-08', '100.00'),
(5, 5, 5, '2019-06-20', '100.00'),
(6, 6, 1, '2019-06-28', '100.00'),
(7, 7, 2, '2019-07-06', '100.00'),
(8, 8, 3, '2019-07-13', '100.00'),
(9, 9, 4, '2019-07-13', '100.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisa_tare`
--

CREATE TABLE `supervisa_tare` (
  `cod_super_tare` smallint(6) NOT NULL,
  `supervisor_tare` smallint(6) NOT NULL,
  `cod_tare` smallint(6) NOT NULL,
  `fech_super_tare` date NOT NULL,
  `nivel_cumpli_tare` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `supervisa_tare`
--

INSERT INTO `supervisa_tare` (`cod_super_tare`, `supervisor_tare`, `cod_tare`, `fech_super_tare`, `nivel_cumpli_tare`) VALUES
(1, 1, 1, '2019-06-26', '100.00'),
(2, 1, 2, '2019-06-20', '50.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE `tarea` (
  `cod_tare` smallint(6) NOT NULL,
  `estado_tare` varchar(20) DEFAULT NULL,
  `descp_tare` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`cod_tare`, `estado_tare`, `descp_tare`) VALUES
(1, 'Finalizado', 'publicacion en redes sociales de la campaña'),
(2, 'Finalizado', 'Solicitar permiso a lugares para realizar campaña de recoleccion de residuos de aparatos elctricos y electronicos'),
(3, 'Finalizado', 'Entrega de volantes a colegios'),
(4, 'Finalizado', 'Entrega de volantes a otras iniversidades'),
(5, 'Finalizado', 'Realizar un pedido a entidades para que brinden un lugar,Equipos de computo para llevar a cabo la compaña de alfabetizacion digital para adultos'),
(6, 'Finalizado', 'Pedir tatos de los asistentes'),
(7, 'Finalizado', 'Sortear premios'),
(8, 'Finalizado', 'Entregar premios'),
(9, 'Finalizado', 'Hacer un test para ver cuanto han aprendido'),
(10, 'Finalizado', 'Solicitar permiso para hacer un Mantenimiento preventivo y correctivo de computadoras de la ESIS de los  laboratorios A,B,C'),
(11, 'Finalizado', 'Comprar de materiales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono_apoy`
--

CREATE TABLE `telefono_apoy` (
  `cod_telef_apoy` varchar(11) NOT NULL,
  `cod_apoy` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono_apoy`
--

INSERT INTO `telefono_apoy` (`cod_telef_apoy`, `cod_apoy`) VALUES
('943854393', 1),
('965823934', 1),
('965883544', 2),
('968305858', 2),
('974947516', 3),
('985344546', 3),
('952038567', 4),
('961414554', 4),
('916999470', 5),
('978888886', 5),
('941451154', 6),
('969558356', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono_auspi`
--

CREATE TABLE `telefono_auspi` (
  `cod_telef_auspi` varchar(11) NOT NULL,
  `cod_auspi` smallint(6) NOT NULL,
  `op_telef_auspi` varchar(20) DEFAULT NULL,
  `tipo_telef_auspi` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono_auspi`
--

INSERT INTO `telefono_auspi` (`cod_telef_auspi`, `cod_auspi`, `op_telef_auspi`, `tipo_telef_auspi`) VALUES
('052-203081', 9, 'Movistar', 'Fijo'),
('052-223895', 2, 'Movistar', 'Fijo'),
('052-235625', 3, 'Claro', 'Fijo'),
('052-343499', 8, 'Movistar', 'Fijo'),
('052-349982', 1, 'Claro', 'Fijo'),
('052-349990', 1, 'Movistar', 'Fijo'),
('052-606980', 10, 'Claro', 'Fijo'),
('052-787867', 7, 'Movistar', 'Fijo'),
('933491855', 5, 'Entel', 'Celular'),
('947339087', 6, 'Movistar', 'Celular'),
('952337291', 4, 'Claro', 'Celular'),
('952338642', 4, 'Movistar', 'Celular');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono_encar`
--

CREATE TABLE `telefono_encar` (
  `cod_telef_encar` varchar(11) NOT NULL,
  `cod_encar` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono_encar`
--

INSERT INTO `telefono_encar` (`cod_telef_encar`, `cod_encar`) VALUES
('952430189', 1),
('952486213', 1),
('952486312', 2),
('952761984', 2),
('952043216', 3),
('954459217', 3),
('954243251', 4),
('954240650', 5),
('954246659', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono_invi`
--

CREATE TABLE `telefono_invi` (
  `cod_telef_invi` varchar(11) NOT NULL,
  `cod_invi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono_invi`
--

INSERT INTO `telefono_invi` (`cod_telef_invi`, `cod_invi`) VALUES
('951685475', 1),
('951685540', 2),
('951665440', 3),
('951698045', 4),
('951665489', 5),
('951321445', 6),
('951665648', 7),
('951698546', 8),
('951665450', 9),
('951654681', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono_part`
--

CREATE TABLE `telefono_part` (
  `cod_telef_part` varchar(15) NOT NULL,
  `cod_part` int(11) NOT NULL,
  `refer_telef_part` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono_part`
--

INSERT INTO `telefono_part` (`cod_telef_part`, `cod_part`, `refer_telef_part`) VALUES
('052001644', 1, 'hija'),
('052014930', 2, 'hijo'),
('052321000', 3, 'sobrina'),
('052321964', 4, 'hija'),
('052406018', 5, 'hijo'),
('052487003', 6, 'hijo'),
('052489620', 7, 'hija'),
('052951431', 11, 'madre'),
('052980031', 10, 'madre'),
('052987121', 9, 'madre'),
('052987431', 8, 'hija');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiene_activ`
--

CREATE TABLE `tiene_activ` (
  `cod_tiene_activ` smallint(6) NOT NULL,
  `cod_activ` smallint(6) NOT NULL,
  `cod_tare` smallint(6) NOT NULL,
  `fech_ini_tare` date NOT NULL,
  `fech_fin_tare` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tiene_activ`
--

INSERT INTO `tiene_activ` (`cod_tiene_activ`, `cod_activ`, `cod_tare`, `fech_ini_tare`, `fech_fin_tare`) VALUES
(1, 1, 1, '2019-04-27', '2019-04-29'),
(2, 1, 2, '2019-04-29', '2019-05-07'),
(3, 1, 3, '2019-05-09', '2019-05-16'),
(4, 1, 4, '2019-05-17', '2019-05-27'),
(5, 2, 1, '2019-06-20', '2019-06-27'),
(6, 2, 2, '2019-06-28', '2019-04-05'),
(7, 2, 3, '2019-07-06', '2019-07-12'),
(8, 2, 4, '2019-07-13', '2019-07-26'),
(9, 3, 1, '2019-06-01', '2019-07-01'),
(10, 3, 2, '2019-07-02', '2019-08-29'),
(11, 1, 1, '2019-04-27', '2019-04-29'),
(12, 2, 8, '2019-10-09', '2019-10-09'),
(13, 9, 10, '0000-00-00', '0000-00-00'),
(14, 8, 11, '2019-07-15', '2019-07-18');

--
-- Disparadores `tiene_activ`
--
DELIMITER $$
CREATE TRIGGER `cant_tareas` AFTER INSERT ON `tiene_activ` FOR EACH ROW BEGIN

UPDATE Actividad set cant_tare=cant_tare+1
WHERE Actividad.cod_activ=NEW.cod_activ;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_activ`
--

CREATE TABLE `tipo_activ` (
  `cod_tipo_activ` smallint(6) NOT NULL,
  `descp_tipo_activ` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_activ`
--

INSERT INTO `tipo_activ` (`cod_tipo_activ`, `descp_tipo_activ`) VALUES
(1, 'Campaña'),
(2, 'Mano de obra'),
(3, 'Social');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_car_apoy`
--

CREATE TABLE `tipo_car_apoy` (
  `cod_ti_car_apoy` smallint(6) NOT NULL,
  `descp_car_apoy` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_car_apoy`
--

INSERT INTO `tipo_car_apoy` (`cod_ti_car_apoy`, `descp_car_apoy`) VALUES
(1, 'Alumno'),
(2, 'Profesor'),
(3, 'Administrativo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_car_encar`
--

CREATE TABLE `tipo_car_encar` (
  `cod_tipo_car_encar` smallint(6) NOT NULL,
  `descp_car_encar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_car_encar`
--

INSERT INTO `tipo_car_encar` (`cod_tipo_car_encar`, `descp_car_encar`) VALUES
(1, 'Director'),
(2, 'Docente'),
(3, 'Administrativo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `contrasenia` int(10) NOT NULL,
  `id_usuario` smallint(30) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`contrasenia`, `id_usuario`, `nombre`) VALUES
(123456, 1, 'juan');

-- --------------------------------------------------------

--
-- Estructura para la vista `mont_finan`
--
DROP TABLE IF EXISTS `mont_finan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `mont_finan`  AS  select `proyecto`.`titu_proy` AS `titu_proy`,`financiamiento`.`fuente_finan` AS `fuente_finan`,`financiamiento`.`monto_finan` AS `monto_finan` from (`financiamiento` join `proyecto` on(`financiamiento`.`cod_proy` = `proyecto`.`cod_proy`)) where `financiamiento`.`monto_finan` > 240 order by `financiamiento`.`monto_finan` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `obj_general`
--
DROP TABLE IF EXISTS `obj_general`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `obj_general`  AS  select `objetivo`.`descp_obj` AS `descp_obj`,`objetivo`.`niv_cumpli` AS `niv_cumpli` from `objetivo` where `objetivo`.`tip_obj` like '%general%' ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`cod_activ`),
  ADD KEY `FK_cod_tipo_activ` (`cod_tipo_activ`);

--
-- Indices de la tabla `antecedente`
--
ALTER TABLE `antecedente`
  ADD PRIMARY KEY (`cod_ante`),
  ADD KEY `FK_codproy_a` (`cod_proy`);

--
-- Indices de la tabla `auspiciador`
--
ALTER TABLE `auspiciador`
  ADD PRIMARY KEY (`cod_auspi`);

--
-- Indices de la tabla `ayuda`
--
ALTER TABLE `ayuda`
  ADD PRIMARY KEY (`cod_ayuda`),
  ADD KEY `FK_codproy_ay` (`cod_proy`),
  ADD KEY `FK_codauspi_ay` (`cod_auspi`);

--
-- Indices de la tabla `beneficia`
--
ALTER TABLE `beneficia`
  ADD PRIMARY KEY (`cod_benefi`),
  ADD KEY `FK_codproy_ben` (`cod_proy`),
  ADD KEY `FK_codbene_ben` (`cod_bene`);

--
-- Indices de la tabla `beneficiaria`
--
ALTER TABLE `beneficiaria`
  ADD PRIMARY KEY (`cod_bene`);

--
-- Indices de la tabla `colaboracion`
--
ALTER TABLE `colaboracion`
  ADD PRIMARY KEY (`cod_colab`),
  ADD KEY `FK_codayuda_col` (`cod_ayuda`);

--
-- Indices de la tabla `correo_auspi`
--
ALTER TABLE `correo_auspi`
  ADD PRIMARY KEY (`cod_co_auspi`),
  ADD KEY `FK_codauspi_corr` (`cod_auspi`);

--
-- Indices de la tabla `dependencia_inter`
--
ALTER TABLE `dependencia_inter`
  ADD PRIMARY KEY (`cod_depen_inter`),
  ADD KEY `FK_codproy_depen` (`cod_proy`);

--
-- Indices de la tabla `ejecutar`
--
ALTER TABLE `ejecutar`
  ADD PRIMARY KEY (`cod_ejec`),
  ADD KEY `FK_cod_invi_ejec` (`cod_invi`),
  ADD KEY `FK_cod_part_ejec` (`cod_part`),
  ADD KEY `FK_cod_lug_ejec` (`cod_lug`),
  ADD KEY `FK_facilitador_ejec` (`facilitador_ejec`),
  ADD KEY `FK_cod_estado_proyecto_ejec` (`cod_estado_proyecto`),
  ADD KEY `FK_cod_activ_ejec` (`cod_activ`);

--
-- Indices de la tabla `eje_rsu`
--
ALTER TABLE `eje_rsu`
  ADD PRIMARY KEY (`id_eje`);

--
-- Indices de la tabla `encargado`
--
ALTER TABLE `encargado`
  ADD PRIMARY KEY (`cod_encar`),
  ADD KEY `FK_cod_tipo_car_encar` (`cod_tipo_car_encar`),
  ADD KEY `FK_cod_grado_acad_encar` (`cod_grado_acad`);

--
-- Indices de la tabla `estado_proyecto`
--
ALTER TABLE `estado_proyecto`
  ADD PRIMARY KEY (`cod_estado_proyecto`);

--
-- Indices de la tabla `financiamiento`
--
ALTER TABLE `financiamiento`
  ADD PRIMARY KEY (`cod_finan`),
  ADD KEY `FK_cod_proy_finan` (`cod_proy`);

--
-- Indices de la tabla `grado_academico`
--
ALTER TABLE `grado_academico`
  ADD PRIMARY KEY (`cod_grado_acad`);

--
-- Indices de la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD PRIMARY KEY (`cod_insti`),
  ADD KEY `FK_cod_pais_insti` (`cod_pais`);

--
-- Indices de la tabla `integrantes_comite`
--
ALTER TABLE `integrantes_comite`
  ADD PRIMARY KEY (`cod_integ`);

--
-- Indices de la tabla `int_com_proyecto`
--
ALTER TABLE `int_com_proyecto`
  ADD PRIMARY KEY (`cod_int_proy`),
  ADD KEY `FK_codinteg_intcom` (`cod_integ`),
  ADD KEY `FK_codproy_intcom` (`cod_proy`);

--
-- Indices de la tabla `invitado`
--
ALTER TABLE `invitado`
  ADD PRIMARY KEY (`cod_invi`),
  ADD KEY `FK_cod_grado_acad_invi` (`cod_grado_acad`);

--
-- Indices de la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`cod_lug`);

--
-- Indices de la tabla `medio_verificacion_activ`
--
ALTER TABLE `medio_verificacion_activ`
  ADD PRIMARY KEY (`cod_med_verif_activ`),
  ADD KEY `FK_cod_super_activ_mva` (`cod_super_activ`);

--
-- Indices de la tabla `medio_verificacion_tare`
--
ALTER TABLE `medio_verificacion_tare`
  ADD PRIMARY KEY (`cod_med_tare`),
  ADD KEY `FK_cod_super_tare` (`cod_super_tare`);

--
-- Indices de la tabla `objetivo`
--
ALTER TABLE `objetivo`
  ADD PRIMARY KEY (`cod_obj`),
  ADD KEY `FK_codproy_obj` (`cod_proy`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`cod_pais`);

--
-- Indices de la tabla `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`cod_part`);

--
-- Indices de la tabla `personal_apoyo`
--
ALTER TABLE `personal_apoyo`
  ADD PRIMARY KEY (`cod_apoy`),
  ADD KEY `FK_cod_ti_car_apoy` (`cod_ti_car_apoy`);

--
-- Indices de la tabla `pertenece_ben`
--
ALTER TABLE `pertenece_ben`
  ADD PRIMARY KEY (`cod_perte_ben`),
  ADD KEY `FK_cod_part_pertenece_ben` (`cod_part`),
  ADD KEY `FK_cod_bene_pertenece_ben` (`cod_bene`);

--
-- Indices de la tabla `pertenece_invi`
--
ALTER TABLE `pertenece_invi`
  ADD PRIMARY KEY (`cod_perte_invi`),
  ADD KEY `FK_cod_invi_pertenece_invi` (`cod_invi`),
  ADD KEY `FK_cod_insti_pertenece_invi` (`cod_insti`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`cod_proy`),
  ADD KEY `fk_eje_proy` (`eje_proy`);

--
-- Indices de la tabla `realiza`
--
ALTER TABLE `realiza`
  ADD PRIMARY KEY (`cod_reali`),
  ADD KEY `FK_cod_activ_re` (`cod_activ`),
  ADD KEY `FK_cod_proy_re` (`cod_proy`);

--
-- Indices de la tabla `recursos`
--
ALTER TABLE `recursos`
  ADD PRIMARY KEY (`cod_recur`);

--
-- Indices de la tabla `recur_activ`
--
ALTER TABLE `recur_activ`
  ADD PRIMARY KEY (`cod_recur_activ`),
  ADD KEY `FK_cod_activ_re_ac` (`cod_activ`),
  ADD KEY `FK_cod_recur_re_ac` (`cod_recur`);

--
-- Indices de la tabla `recur_tare`
--
ALTER TABLE `recur_tare`
  ADD PRIMARY KEY (`cod_recur_tare`),
  ADD KEY `FK_cod_tare_re_ta` (`cod_tare`),
  ADD KEY `FK_cod_recur_re_ta` (`cod_recur`);

--
-- Indices de la tabla `requerir`
--
ALTER TABLE `requerir`
  ADD PRIMARY KEY (`cod_requi`),
  ADD KEY `FK_cod_tare` (`cod_tare`),
  ADD KEY `FK_cod_apoy` (`cod_apoy`);

--
-- Indices de la tabla `supervisa_activ`
--
ALTER TABLE `supervisa_activ`
  ADD PRIMARY KEY (`cod_super_activ`),
  ADD KEY `FK_supervisor_activ_sa` (`supervisor_activ`),
  ADD KEY `FK_cod_activ_sa` (`cod_activ`);

--
-- Indices de la tabla `supervisa_tare`
--
ALTER TABLE `supervisa_tare`
  ADD PRIMARY KEY (`cod_super_tare`),
  ADD KEY `FK_supervisor_tare_su_ta` (`supervisor_tare`),
  ADD KEY `FK_cod_tare_su_ta` (`cod_tare`);

--
-- Indices de la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD PRIMARY KEY (`cod_tare`);

--
-- Indices de la tabla `telefono_apoy`
--
ALTER TABLE `telefono_apoy`
  ADD PRIMARY KEY (`cod_telef_apoy`),
  ADD KEY `FK_cod_apoy_te_a` (`cod_apoy`);

--
-- Indices de la tabla `telefono_auspi`
--
ALTER TABLE `telefono_auspi`
  ADD PRIMARY KEY (`cod_telef_auspi`),
  ADD KEY `FK_codauspi_tel` (`cod_auspi`);

--
-- Indices de la tabla `telefono_encar`
--
ALTER TABLE `telefono_encar`
  ADD PRIMARY KEY (`cod_telef_encar`),
  ADD KEY `FK_cod_encar_te` (`cod_encar`);

--
-- Indices de la tabla `telefono_invi`
--
ALTER TABLE `telefono_invi`
  ADD PRIMARY KEY (`cod_telef_invi`),
  ADD KEY `FK_cod_invi_ti` (`cod_invi`);

--
-- Indices de la tabla `telefono_part`
--
ALTER TABLE `telefono_part`
  ADD PRIMARY KEY (`cod_telef_part`),
  ADD KEY `FK_cod_part_tp` (`cod_part`);

--
-- Indices de la tabla `tiene_activ`
--
ALTER TABLE `tiene_activ`
  ADD PRIMARY KEY (`cod_tiene_activ`),
  ADD KEY `FK_cod_activ_ti_a` (`cod_activ`),
  ADD KEY `FK_cod_tare_ti_a` (`cod_tare`);

--
-- Indices de la tabla `tipo_activ`
--
ALTER TABLE `tipo_activ`
  ADD PRIMARY KEY (`cod_tipo_activ`);

--
-- Indices de la tabla `tipo_car_apoy`
--
ALTER TABLE `tipo_car_apoy`
  ADD PRIMARY KEY (`cod_ti_car_apoy`);

--
-- Indices de la tabla `tipo_car_encar`
--
ALTER TABLE `tipo_car_encar`
  ADD PRIMARY KEY (`cod_tipo_car_encar`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `cod_activ` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `antecedente`
--
ALTER TABLE `antecedente`
  MODIFY `cod_ante` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `auspiciador`
--
ALTER TABLE `auspiciador`
  MODIFY `cod_auspi` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `ayuda`
--
ALTER TABLE `ayuda`
  MODIFY `cod_ayuda` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `beneficia`
--
ALTER TABLE `beneficia`
  MODIFY `cod_benefi` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `beneficiaria`
--
ALTER TABLE `beneficiaria`
  MODIFY `cod_bene` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `colaboracion`
--
ALTER TABLE `colaboracion`
  MODIFY `cod_colab` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `correo_auspi`
--
ALTER TABLE `correo_auspi`
  MODIFY `cod_co_auspi` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `dependencia_inter`
--
ALTER TABLE `dependencia_inter`
  MODIFY `cod_depen_inter` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `ejecutar`
--
ALTER TABLE `ejecutar`
  MODIFY `cod_ejec` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `eje_rsu`
--
ALTER TABLE `eje_rsu`
  MODIFY `id_eje` smallint(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `encargado`
--
ALTER TABLE `encargado`
  MODIFY `cod_encar` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estado_proyecto`
--
ALTER TABLE `estado_proyecto`
  MODIFY `cod_estado_proyecto` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `financiamiento`
--
ALTER TABLE `financiamiento`
  MODIFY `cod_finan` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `grado_academico`
--
ALTER TABLE `grado_academico`
  MODIFY `cod_grado_acad` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `institucion`
--
ALTER TABLE `institucion`
  MODIFY `cod_insti` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `integrantes_comite`
--
ALTER TABLE `integrantes_comite`
  MODIFY `cod_integ` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `int_com_proyecto`
--
ALTER TABLE `int_com_proyecto`
  MODIFY `cod_int_proy` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `invitado`
--
ALTER TABLE `invitado`
  MODIFY `cod_invi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `lugar`
--
ALTER TABLE `lugar`
  MODIFY `cod_lug` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `medio_verificacion_activ`
--
ALTER TABLE `medio_verificacion_activ`
  MODIFY `cod_med_verif_activ` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `medio_verificacion_tare`
--
ALTER TABLE `medio_verificacion_tare`
  MODIFY `cod_med_tare` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `objetivo`
--
ALTER TABLE `objetivo`
  MODIFY `cod_obj` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `cod_pais` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `participante`
--
ALTER TABLE `participante`
  MODIFY `cod_part` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `personal_apoyo`
--
ALTER TABLE `personal_apoyo`
  MODIFY `cod_apoy` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `pertenece_ben`
--
ALTER TABLE `pertenece_ben`
  MODIFY `cod_perte_ben` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `pertenece_invi`
--
ALTER TABLE `pertenece_invi`
  MODIFY `cod_perte_invi` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `cod_proy` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `realiza`
--
ALTER TABLE `realiza`
  MODIFY `cod_reali` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `recursos`
--
ALTER TABLE `recursos`
  MODIFY `cod_recur` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `recur_activ`
--
ALTER TABLE `recur_activ`
  MODIFY `cod_recur_activ` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `recur_tare`
--
ALTER TABLE `recur_tare`
  MODIFY `cod_recur_tare` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `requerir`
--
ALTER TABLE `requerir`
  MODIFY `cod_requi` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `supervisa_activ`
--
ALTER TABLE `supervisa_activ`
  MODIFY `cod_super_activ` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `supervisa_tare`
--
ALTER TABLE `supervisa_tare`
  MODIFY `cod_super_tare` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tarea`
--
ALTER TABLE `tarea`
  MODIFY `cod_tare` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tiene_activ`
--
ALTER TABLE `tiene_activ`
  MODIFY `cod_tiene_activ` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `tipo_activ`
--
ALTER TABLE `tipo_activ`
  MODIFY `cod_tipo_activ` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_car_apoy`
--
ALTER TABLE `tipo_car_apoy`
  MODIFY `cod_ti_car_apoy` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_car_encar`
--
ALTER TABLE `tipo_car_encar`
  MODIFY `cod_tipo_car_encar` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` smallint(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `FK_cod_tipo_activ` FOREIGN KEY (`cod_tipo_activ`) REFERENCES `tipo_activ` (`cod_tipo_activ`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `antecedente`
--
ALTER TABLE `antecedente`
  ADD CONSTRAINT `FK_codproy_a` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ayuda`
--
ALTER TABLE `ayuda`
  ADD CONSTRAINT `FK_codauspi_ay` FOREIGN KEY (`cod_auspi`) REFERENCES `auspiciador` (`cod_auspi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_codproy_ay` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `beneficia`
--
ALTER TABLE `beneficia`
  ADD CONSTRAINT `FK_codbene_ben` FOREIGN KEY (`cod_bene`) REFERENCES `beneficiaria` (`cod_bene`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_codproy_ben` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `colaboracion`
--
ALTER TABLE `colaboracion`
  ADD CONSTRAINT `FK_codayuda_col` FOREIGN KEY (`cod_ayuda`) REFERENCES `ayuda` (`cod_ayuda`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `correo_auspi`
--
ALTER TABLE `correo_auspi`
  ADD CONSTRAINT `FK_codauspi_corr` FOREIGN KEY (`cod_auspi`) REFERENCES `auspiciador` (`cod_auspi`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dependencia_inter`
--
ALTER TABLE `dependencia_inter`
  ADD CONSTRAINT `FK_codproy_depen` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ejecutar`
--
ALTER TABLE `ejecutar`
  ADD CONSTRAINT `FK_cod_activ_ejec` FOREIGN KEY (`cod_activ`) REFERENCES `actividad` (`cod_activ`),
  ADD CONSTRAINT `FK_cod_estado_proyecto_ejec` FOREIGN KEY (`cod_estado_proyecto`) REFERENCES `estado_proyecto` (`cod_estado_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_invi_ejec` FOREIGN KEY (`cod_invi`) REFERENCES `invitado` (`cod_invi`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_lug_ejec` FOREIGN KEY (`cod_lug`) REFERENCES `lugar` (`cod_lug`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_part_ejec` FOREIGN KEY (`cod_part`) REFERENCES `participante` (`cod_part`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_facilitador_ejec` FOREIGN KEY (`facilitador_ejec`) REFERENCES `encargado` (`cod_encar`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `encargado`
--
ALTER TABLE `encargado`
  ADD CONSTRAINT `FK_cod_grado_acad_encar` FOREIGN KEY (`cod_grado_acad`) REFERENCES `grado_academico` (`cod_grado_acad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_tipo_car_encar` FOREIGN KEY (`cod_tipo_car_encar`) REFERENCES `tipo_car_encar` (`cod_tipo_car_encar`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `financiamiento`
--
ALTER TABLE `financiamiento`
  ADD CONSTRAINT `FK_cod_proy_finan` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD CONSTRAINT `FK_cod_pais_insti` FOREIGN KEY (`cod_pais`) REFERENCES `pais` (`cod_pais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `int_com_proyecto`
--
ALTER TABLE `int_com_proyecto`
  ADD CONSTRAINT `FK_codinteg_intcom` FOREIGN KEY (`cod_integ`) REFERENCES `integrantes_comite` (`cod_integ`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_codproy_intcom` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `invitado`
--
ALTER TABLE `invitado`
  ADD CONSTRAINT `FK_cod_grado_acad_invi` FOREIGN KEY (`cod_grado_acad`) REFERENCES `grado_academico` (`cod_grado_acad`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `medio_verificacion_activ`
--
ALTER TABLE `medio_verificacion_activ`
  ADD CONSTRAINT `FK_cod_super_activ_mva` FOREIGN KEY (`cod_super_activ`) REFERENCES `supervisa_activ` (`cod_super_activ`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `medio_verificacion_tare`
--
ALTER TABLE `medio_verificacion_tare`
  ADD CONSTRAINT `FK_cod_super_tare` FOREIGN KEY (`cod_super_tare`) REFERENCES `supervisa_tare` (`cod_super_tare`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `objetivo`
--
ALTER TABLE `objetivo`
  ADD CONSTRAINT `FK_codproy_obj` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_apoyo`
--
ALTER TABLE `personal_apoyo`
  ADD CONSTRAINT `FK_cod_ti_car_apoy` FOREIGN KEY (`cod_ti_car_apoy`) REFERENCES `tipo_car_apoy` (`cod_ti_car_apoy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pertenece_ben`
--
ALTER TABLE `pertenece_ben`
  ADD CONSTRAINT `FK_cod_bene_pertenece_ben` FOREIGN KEY (`cod_bene`) REFERENCES `beneficiaria` (`cod_bene`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_part_pertenece_ben` FOREIGN KEY (`cod_part`) REFERENCES `participante` (`cod_part`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pertenece_invi`
--
ALTER TABLE `pertenece_invi`
  ADD CONSTRAINT `FK_cod_insti_pertenece_invi` FOREIGN KEY (`cod_insti`) REFERENCES `institucion` (`cod_insti`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_invi_pertenece_invi` FOREIGN KEY (`cod_invi`) REFERENCES `invitado` (`cod_invi`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_eje_proy` FOREIGN KEY (`eje_proy`) REFERENCES `eje_rsu` (`id_eje`);

--
-- Filtros para la tabla `realiza`
--
ALTER TABLE `realiza`
  ADD CONSTRAINT `FK_cod_activ_re` FOREIGN KEY (`cod_activ`) REFERENCES `actividad` (`cod_activ`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_proy_re` FOREIGN KEY (`cod_proy`) REFERENCES `proyecto` (`cod_proy`);

--
-- Filtros para la tabla `recur_activ`
--
ALTER TABLE `recur_activ`
  ADD CONSTRAINT `FK_cod_activ_re_ac` FOREIGN KEY (`cod_activ`) REFERENCES `actividad` (`cod_activ`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_recur_re_ac` FOREIGN KEY (`cod_recur`) REFERENCES `recursos` (`cod_recur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recur_tare`
--
ALTER TABLE `recur_tare`
  ADD CONSTRAINT `FK_cod_recur_re_ta` FOREIGN KEY (`cod_recur`) REFERENCES `recursos` (`cod_recur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_tare_re_ta` FOREIGN KEY (`cod_tare`) REFERENCES `tarea` (`cod_tare`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `requerir`
--
ALTER TABLE `requerir`
  ADD CONSTRAINT `FK_cod_apoy` FOREIGN KEY (`cod_apoy`) REFERENCES `personal_apoyo` (`cod_apoy`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_tare` FOREIGN KEY (`cod_tare`) REFERENCES `tarea` (`cod_tare`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `supervisa_activ`
--
ALTER TABLE `supervisa_activ`
  ADD CONSTRAINT `FK_cod_activ_sa` FOREIGN KEY (`cod_activ`) REFERENCES `actividad` (`cod_activ`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_supervisor_activ_sa` FOREIGN KEY (`supervisor_activ`) REFERENCES `encargado` (`cod_encar`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `supervisa_tare`
--
ALTER TABLE `supervisa_tare`
  ADD CONSTRAINT `FK_cod_tare_su_ta` FOREIGN KEY (`cod_tare`) REFERENCES `tarea` (`cod_tare`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_supervisor_tare_su_ta` FOREIGN KEY (`supervisor_tare`) REFERENCES `encargado` (`cod_encar`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefono_apoy`
--
ALTER TABLE `telefono_apoy`
  ADD CONSTRAINT `FK_cod_apoy_te_a` FOREIGN KEY (`cod_apoy`) REFERENCES `personal_apoyo` (`cod_apoy`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefono_auspi`
--
ALTER TABLE `telefono_auspi`
  ADD CONSTRAINT `FK_codauspi_tel` FOREIGN KEY (`cod_auspi`) REFERENCES `auspiciador` (`cod_auspi`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefono_encar`
--
ALTER TABLE `telefono_encar`
  ADD CONSTRAINT `FK_cod_encar_te` FOREIGN KEY (`cod_encar`) REFERENCES `encargado` (`cod_encar`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefono_invi`
--
ALTER TABLE `telefono_invi`
  ADD CONSTRAINT `FK_cod_invi_ti` FOREIGN KEY (`cod_invi`) REFERENCES `invitado` (`cod_invi`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefono_part`
--
ALTER TABLE `telefono_part`
  ADD CONSTRAINT `FK_cod_part_tp` FOREIGN KEY (`cod_part`) REFERENCES `participante` (`cod_part`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tiene_activ`
--
ALTER TABLE `tiene_activ`
  ADD CONSTRAINT `FK_cod_activ_ti_a` FOREIGN KEY (`cod_activ`) REFERENCES `actividad` (`cod_activ`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_cod_tare_ti_a` FOREIGN KEY (`cod_tare`) REFERENCES `tarea` (`cod_tare`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
