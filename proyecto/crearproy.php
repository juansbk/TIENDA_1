<!DOCTYPE html>
<html lang="en">
      <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Crear Proyecto</title>
      </head>

      <style>
            body {
                  background-color: #E5E7E9;
            }
            p {
                  background-color: #FFFFFF);
            }
      </style>

      <body>   
            <h1 class="page-header" align="center" p>Nuevo Proyecto</h1>
            <ol class="breadcrumb">
                  <a href="menuproy.php">Ir a inicio</a></li><br>
                  <a href="mostrarproy.php">Ver lista de proyecto</a></li>
            </ol>

            <form id="frm-proyecto" action="../modelos/proyectoModelo.php" method="POST" >
                  <div  align="center" style="border: 2px solid black; margin:10px 400px;">
                  <br><br>
                  <label>Título:</label>
                  <input type="string" name="titu_proy" id="titu_proy" class="form-control" placeholder="Ingrese titulo" >
                  <br><br>

                  <label>Justificación:</label>
                  <input  type="text" name="justi_proy" id="justi_proy"  class="form-control" placeholder="Ingrese justificación" >
                  <br><br>

                  <label>Ámbito:</label>
                  <input type="text" name="ambi_proy" id="ambi_proy" class="form-control" placeholder="Ingrese ámbito">
                  <br><br>

                  <label>Población:</label>
                  <input type="text" name="pobla_proy" id="pobla_proy"  class="form-control" placeholder="Ingrese población">
                  <br><br>

                  <label>Dirigido a:</label>
                  <input type="text" name="dirig_proy" id="dirig_proy" class="form-control" placeholder="Ingrese a quién va dirigido">
                  <br><br>

                  <label>Documento de validación:</label>
                  <input type="text" name="doc_val_proy" id="doc_val_proy" class="form-control" placeholder="Ingrese validación">
                  <br><br>

                  <label>Presupuesto:</label>
                  <input type="float" name="presu_proy" id="presu_proy" class="form-control" placeholder="Ingrese presupuesto">
                  <br><br>

                  <label>Metodología:</label>
                  <input type="text" name="metod_proy" id="metod_proy" class="form-control" placeholder="Ingrese metodología">
                  <br><br>

                  <label>Eje:</label>  
                  <?php
                        include("../modelos/database.php");
                        $consulta="select id_eje,descp_eje from eje_rsu";
                        $trae=mysqli_query($con,$consulta);
                  ?>
                  <select type="select" name="eje_proy" id="eje_proy" class="form-control">
                  <?php
                        while($ej=mysqli_fetch_array($trae)){//convierte para poder utilizar -> convierte en arrayimprime los arrays
                  ?>
                        <option value="<?=$ej['id_eje']?>"><?=$ej['descp_eje']?> </option>
                  <?php
                        }
                  ?>
                        </select>                        
                  <br><br>

                  <label>Fecha inicio</label>
                  <input type="date" name="fe_ini_proy" id="fe_ini_proy" class="form-control" placeholder="Ingrese fecha inicio">
                  <br><br>

                  <label>Fecha fin</label>
                  <input type="date" name="fe_fin_proy" id="fe_fin_proy" " class="form-control" placeholder="Ingrese fecha fin">
                  <br><br>
                  <label>Diagnóstico</label>
                  <input type="text" name="diag_proy" id="diag_proy" class="form-control" placeholder="Ingrese diagnóstico">
                  <br><br>

                  <label>Resultado</label>
                  <input type="float" name="resul_proy" id="resul_proy  class="form-control" placeholder="Ingrese resultado">
                  <br><br>
                  
                  <input type="submit" id="Agregar" value="Agregar" class="btn-eviar">
                  <br><br>
                  </div>
            </form>                  
      </body>
</html>
