<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Lista de Proyectos</title>
  </head>
  <style>
    body{
      background-color: #E5E7E9;
    }
    h1{
      background-color: #006699;
      color: #FFFFFF;
      text-align:center;
    }
    p{
      background-color: #FFFFFF);
    }
    .titulotabla {
      Font-size: 11pt;
      font-weight:bold;
      background:#006699;
      color: #FFFFFF;
      text-transform: uppercase;
    }
    table{
      Font-size: 10pt;
      border-radius:1px;
      border:3px solid;
      background:#FDEBD0;
      color:#00121c;
      width: 70%;
    }    
    #Lineadivide{
      background:#EEEEEE;
      color:#263238;
    }    
  </style>
  <body>
    <div align="left">
      <a  href="menuproy.php">Ir a Inicio</a>
    </div>

    <div align="right">
      <a  href="../reportes/R.mostrarproy.php">Imprimir</a>
    </div>

    <div class="col-md-8">    
      <h1>LISTADO DE PROYECTOS</h1>
      <table align="center">
        <tr class='titulotabla' align="center"> 
          <td>Título</td>
          <td>Documento validación</td>
          <td>Fecha inicio</td>
          <td>Fecha fin</td>
          <td>&nbsp;</td>
        </tr>   
        <?php
          include("../modelos/database.php");  
          //include("../controlador/proyectoControlador.php");      
        ?>

        <?php
          $mostrar_datos="SELECT * FROM  Proyecto ";
          $ejecutar_mostrar=mysqli_query($con,$mostrar_datos);
            
          while($mostrar=mysqli_fetch_array($ejecutar_mostrar)){  
            //foreach ($ejecutar_mostrar as $mostrar_datos){
          // echo var_dump($mostrar);
        ?>
        <tr>
          <td><?php echo $mostrar["titu_proy"]?></td>
          <td><?php echo $mostrar['doc_val_proy']?></td>
          <td><?php echo $mostrar['fe_ini_proy'] ?></td>
          <td><?php echo $mostrar['fe_fin_proy'] ?></td>
        </tr>            
        <?php
          }            
        ?>
    </div>
  </body>
</html>