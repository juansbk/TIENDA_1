<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Menú</title>
  </head>

  <style>
    body {
      background-color: #E5E7E9;
    }
    p {
      background-color: #FFFFFF);}
    }
  </style>

  <body>
    <h1 align="center">Inicio</h1>

    <h2>Menú de proyectos</h2>
    <ol>
      <li><a href="crearproy.php">Nuevo proyecto</a></li>
      <li><a href="mostrarproy.php">Mostrar listado de proyectos</a></li>
    </ol>
    
    <h2>Búsquedas</h2>
    <ol>
      <p>Buscar proyectos según su eje</p>
      <ol>
        <li><a href="../controlador/controladorEje/controlador_eje_gestion_insitucional.php">Gestión Institucional</a></li>
        <li><a href="../controlador/controladorEje/controlador_eje_formacion.php">Formación</a></li>
        <li><a href="../controlador/controladorEje/controlador_eje_investigacion.php">Investigación</a></li>
        <li><a href="../controlador/controladorEje/controlador_eje_participacion_social.php">Participación Social</a></li>
      </ol>

      <p>Buscar personal de apoyo según su cargo</p>
      <ol>
        <li><a href="../controlador/controlador_p_apoyo/controlador_p_apoyo_alumno.php"">Alumno</a></li>
        <li><a href="../controlador/controlador_p_apoyo/controlador_p_apoyo_profesor.php"">Profesor</a></li>
        <li><a href="../controlador/controlador_p_apoyo/controlador_p_apoyo_administrativo.php"">Administrativo</a></li>         
      </ol>

      <p>Buscar participantes de entidades beneficiarias</p>
      <ol>
        <li><a href="../controlador/controladorListaBeneficiaria/controladorListaBeneficiaria.php">Lista de beneficiaria</a></li>
      </ol>
    </ol>    
  </body>
</html>